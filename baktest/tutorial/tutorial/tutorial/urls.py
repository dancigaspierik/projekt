from django.conf.urls import url, include
from rest_framework import routers
from quickstart import views
from django.urls import include, path

#router = routers.DefaultRouter()
#router.register(r'users', views.UserViewSet)
#router.register(r'users',views.UserViewSet)
# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    #url(r'^', include(router.urls)),
    #url(r'^users',views.UserViewSet,)
    #url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    #vráti vsetky meteostanice s ich atributmi(id, name, latitude, longitude)
    url(r'^dataall/', views.get, name='urlname'),
    #vrati atributy najblizsej stanice k suradniciam, frekvency-kolko zaznamov za 1 hodinu(tie sa dopocitaju)
    url(r'^data/$', views.register, name='urlnamee'),
    #graficke^
    url(r'^m1/$',views.m1, name='m1'),
    #vrati atributy najblizsej stanice k suradniciam, frekvency-kolko zaznamov za 1 den(ak nie su tak sa dopocitaju)
    url(r'^data2/$',views.get_data, name='getData'),
    #graficke^
    url(r'^m2/$',views.m2, name='m2'),
    #priestorova interpolacia z n najblizsich stanic
    url(r'^data3/$',views.multiple, name='getmeteo'),
    #graficke^
    url(r'^m3/$',views.m3, name='m3'),
    #dopocitavanie dat pomocou metody simple kriging
    url(r'^krig/$',views.simpleKrig, name='krig'),
    #graficke^
    url(r'^m4/$',views.m4, name='m4'),
    url(r'^m5/$',views.m5, name='m5'),
    url(r'^m6/$',views.m6, name='m6'),
    url(r'^m8/$',views.m8, name='m8'),
    url(r'^m9/$',views.m9, name='m9'),
    url(r'^m10/$',views.m10, name='m10'),
    url(r'^m11/$',views.m11, name='m11'),
    url(r'^m12/$',views.m12, name='m12'),
    url(r'^m13/$',views.m13, name='m13'),
    #vrati atributy najblizsej stanice k suradniciam, frekvency-kolko zaznamov za 1 hodinu(tie sa dopocitaju) + pracuje aj ked data nie su konzistentne(aj Null hodnoty aj cisla)
    url(r'^newdata/$',views.newdata, name='newdata'),
    #hlavne menu(zatial len jednoduchy vyber)
    url(r'^menu/$',views.menu, name='menu'),
    #metoda najblizsieho suseda
    url(r'^nearneigh/$',views.nearneigh, name='nearneigh'),
    #metoda prirodzeneho suseda
    url(r'^naturneigh/$',views.naturneigh, name='naturneigh'),
    #metoda inverznej vazenej vzdialenosti
    url(r'^idw/$',views.idw, name='idw'),
    url(r'^lalala/$',views.lalala, name='lalala'),
    #vrati mapu meteorologickych stanic
    url(r'^stations/$',views.stations, name='stations'),
    url(r'^splinemap/$',views.splinemap, name='splinemap'),
    url(r'^ordkrigmap/$',views.ordkrigmap, name='ordkrigmap'),
    url(r'^unikrigmap/$',views.unikrigmap, name='unikrigmap'),
    url(r'^voronoi/$',views.voronoi, name='voronoi'),
    url(r'^linreg/$',views.linreg, name='linreg'),
    url(r'^polyreg/$',views.polyreg, name='polyreg'),
    #path('users/',views.get,)
]
