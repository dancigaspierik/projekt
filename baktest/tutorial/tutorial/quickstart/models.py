from django.db import models

# Create your models here.
class Measurement(models.Model):
    id = models.IntegerField(primary_key=True)
    station = models.ForeignKey('MeteoStations', models.DO_NOTHING, blank=True, null=True)
    measurement_date = models.IntegerField(blank=True, null=True)
    measurement_hour = models.IntegerField(blank=True, null=True)
    wind_direction = models.IntegerField(blank=True, null=True)
    average_wind_speed = models.IntegerField(blank=True, null=True)
    temperature = models.IntegerField(blank=True, null=True)
    duration_of_sunshine = models.IntegerField(blank=True, null=True)
    global_radiation = models.IntegerField(blank=True, null=True)
    duration_of_precipitation = models.IntegerField(blank=True, null=True)
    air_pressure = models.IntegerField(blank=True, null=True)
    horizontal_view = models.IntegerField(blank=True, null=True)
    overcast = models.IntegerField(blank=True, null=True)
    humidity = models.IntegerField(blank=True, null=True)
    fog = models.IntegerField(blank=True, null=True)
    rain = models.IntegerField(blank=True, null=True)
    snow = models.IntegerField(blank=True, null=True)
    thunderstorm = models.IntegerField(blank=True, null=True)
    ice_formation = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'measurement'

class MeteoStations(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=100, blank=True, null=True)
    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'meteo_stations'
