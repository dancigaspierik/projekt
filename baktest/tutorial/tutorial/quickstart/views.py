from django.shortcuts import render
from rest_framework.decorators import api_view
# Create your views here.
from rest_framework import viewsets
from quickstart.models import *
#from quickstart.models import Measurement
from django.core import serializers
#from quickstart.serializers import UserSerializer
#from quickstart.serializers import Meteo_stationSerializer
from django.http import HttpResponse
from django.http import JsonResponse
from math import sqrt
import json
import numpy as np
import matplotlib as mpl
import matplotlib.cm as cm

#class UserViewSet(viewsets.ModelViewSet):
#    """
#    API endpoint that allows users to be viewed or edited.
#    queryset = MeteoStations.objects.all()
#    queryset = User.objects.filter(username="Marek")
#    if request.method =='GET':
#        queryset=User.objects.filter(username="Marek")
#    else:"""

#    queryset=User.objects.all()
#    serializer_class = UserSerializer
#@api_view(['GET','POST'])

#vrati vsetky meteostanice s ich atributmi(id, name, latitude, longitude)
def get(request):
    queryset = MeteoStations.objects.all()
    data = list(queryset.values())
    return JsonResponse(data,safe=False)
#vrati atributy najblizsej stanice k suradniciam, frekvency-kolko zaznamov za 1 hodinu(tie sa dopocitaju)
def register(request):
    time_from = request.GET["time_from"]
    time_to = request.GET["time_to"]
    lat = float(request.GET["latitude"])
    longi = float(request.GET["longitude"])
    freq = float(request.GET["frequency"])
    tempa = MeteoStations.objects.get(id=210)
#vzdialenosti stanic od suradnic
    temp = sqrt((tempa.latitude - lat)*(tempa.latitude - lat) + (tempa.longitude - longi)*(tempa.longitude - longi))
    tempc = 210
#hladanie najblizsej stanice
    queryset = MeteoStations.objects.filter(id__gte=lat,id__lte=longi)
    for e in MeteoStations.objects.all():
        a = sqrt((e.latitude - lat)*(e.latitude - lat) + (e.longitude - longi)*(e.longitude - longi))
        if a < temp:
            tempa = e
            temp = a
            tempc = e.id
    queryset = Measurement.objects.filter(station_id=tempc,measurement_date__gte=time_from,measurement_date__lte=time_to)
    secondqueryset = Measurement.objects.filter(station_id=tempc,measurement_date=time_from,measurement_hour=1)
    data = []
    finlist = []
    listdatumov =list()
    queryss = Measurement.objects.filter(station_id=tempc,measurement_date__gte=time_from,measurement_date__lte=time_to,measurement_hour=1)
#dopocitavanie udajov pomocou frekvencie   
    for e in queryss:
        finlist.append(Measurement.objects.get(id=e.id-1))
        h=Measurement.objects.get(id=e.id-1)
        for j in range(1, 25):
            eplus= Measurement.objects.get(id=h.id+1)
            for k in range(1, int(freq)):
                tid=h.id
                tstation_id=h.station_id
                tmeasurement_date=h.measurement_date
                tmeasurement_hour=(j-1)+k/freq
                twind_direction=h.wind_direction+((eplus.wind_direction-h.wind_direction)*(k/freq))
                ta_w_s=h.average_wind_speed+((eplus.average_wind_speed-h.average_wind_speed)*(k/freq))
                ttemperature=h.temperature+((eplus.temperature-h.temperature)*(k/freq))
                td_o_s=h.duration_of_sunshine+((eplus.duration_of_sunshine-h.duration_of_sunshine)*(k/freq))
                tg_r=h.global_radiation+((eplus.global_radiation-h.global_radiation)*(k/freq))
                td_o_p=h.duration_of_precipitation+((eplus.duration_of_precipitation-h.duration_of_precipitation)*(k/freq))
                ta_p=h.air_pressure+((eplus.air_pressure-h.air_pressure)*(k/freq))
                th_v=h.horizontal_view+((eplus.horizontal_view-h.horizontal_view)*(k/freq))
                tovercast=h.overcast+((eplus.overcast-h.overcast)*(k/freq))
                thumidity=h.humidity+((eplus.humidity-h.humidity)*(k/freq))
                tfog=h.fog+((eplus.fog-h.fog)*(k/freq))
                train=h.rain+((eplus.rain-h.rain)*(k/freq))
                tsnow=h.snow+((eplus.snow-h.snow)*(k/freq))
                tthunderstorm=h.thunderstorm+((eplus.thunderstorm-h.thunderstorm)*(k/freq))
                ti_f=h.ice_formation+((eplus.ice_formation-h.ice_formation)*(k/freq))
                tm = Measurement(id=tid,station_id=tstation_id,measurement_date=tmeasurement_date,measurement_hour=tmeasurement_hour,wind_direction=twind_direction,average_wind_speed=ta_w_s,temperature=ttemperature,duration_of_sunshine=td_o_s,global_radiation=tg_r,duration_of_precipitation=td_o_p,air_pressure=ta_p,horizontal_view=th_v,overcast=tovercast,humidity=thumidity,fog=tfog,rain=train,snow=tsnow,thunderstorm=tthunderstorm,ice_formation=ti_f)
                finlist.append(tm)
                h = eplus
            finlist.append(Measurement.objects.get(id=h.id))

    testser = serializers.serialize('json',finlist)
    struct = json.loads(testser)
    testser = json.dumps(struct)
    querysetfin = Measurement.objects.filter(id__in=data)
    datafin = list(querysetfin.values())
    stest=1
    s = "[{\"rain\": "+str(stest)+",\"fog\": "+str(stest)+"}]"
    return HttpResponse(testser)
#vrati atributy najblizsej stanice k suradniciam, frekvency-kolko zaznamov za 1 den(ak nie su tak sa dopocitaju)
def get_data(request):
    time_from = request.GET["time_from"]
    time_to = request.GET["time_to"]
    lat = float(request.GET["latitude"])
    longi = float(request.GET["longitude"])
    freq = float(request.GET["frequency"])
#vzdialenosti stanic od suradnic
    tempa = MeteoStations.objects.get(id=210)
    temp = sqrt((tempa.latitude - lat)*(tempa.latitude - lat) + (tempa.longitude - longi)*(tempa.longitude - longi))
    tempc = 210
    queryset = MeteoStations.objects.filter(id__gte=lat,id__lte=longi)
#hladanie najblizsej stanice
    for e in MeteoStations.objects.all():
        a = sqrt((e.latitude - lat)*(e.latitude - lat) + (e.longitude - longi)*(e.longitude - longi))
        if a < temp:
            tempa = e
            temp = a
            tempc = e.id
    queryset = Measurement.objects.filter(station_id=tempc,measurement_date__gte=time_from,measurement_date__lte=time_to)
    secondqueryset = Measurement.objects.filter(station_id=tempc,measurement_date=time_from,measurement_hour=1)
    data = []
    finlist = []
#dopocitanie udajov podla frekvencie
    queryss = Measurement.objects.filter(station_id=tempc,measurement_date__gte=time_from,measurement_date__lte=time_to,measurement_hour=1)
    finlist.append(Measurement.objects.get(station_id=tempc,measurement_date=int(time_from)-1,measurement_hour=24))
    for e in queryss:
        h=Measurement.objects.get(id=e.id)
        for j in range(0, int(24/ freq)):
            eplus= Measurement.objects.get(id=h.id+1)
            tid=h.id
            tstation_id=h.station_id
            tmeasurement_date=h.measurement_date
            tmeasurement_hour=float(j+1)*freq
            twind_direction=h.wind_direction+((eplus.wind_direction-h.wind_direction)*(float((j+1)*freq)-int((j+1)*freq)))
            ta_w_s=h.average_wind_speed+((eplus.average_wind_speed-h.average_wind_speed)*(float((j+1)*freq)-int((j+1)*freq)))
            ttemperature=h.temperature+((eplus.temperature-h.temperature)*(float((j+1)*freq)-int((j+1)*freq)))
            td_o_s=h.duration_of_sunshine+((eplus.duration_of_sunshine-h.duration_of_sunshine)*(float((j+1)*freq)-int((j+1)*freq)))
            tg_r=h.global_radiation+((eplus.global_radiation-h.global_radiation)*(float((j+1)*freq)-int((j+1)*freq)))
            td_o_p=h.duration_of_precipitation+((eplus.duration_of_precipitation-h.duration_of_precipitation)*(float((j+1)*freq)-int((j+1)*freq)))
            ta_p=h.air_pressure+((eplus.air_pressure-h.air_pressure)*(float((j+1)*freq)-int((j+1)*freq)))
            th_v=h.horizontal_view+((eplus.horizontal_view-h.horizontal_view)*(float((j+1)*freq)-int((j+1)*freq)))
            tovercast=h.overcast+((eplus.overcast-h.overcast)*(float((j+1)*freq)-int((j+1)*freq)))
            thumidity=h.humidity+((eplus.humidity-h.humidity)*(float((j+1)*freq)-int((j+1)*freq)))
            tfog=h.fog+((eplus.fog-h.fog)*(float((j+1)*freq)-int((j+1)*freq)))
            train=h.rain+((eplus.rain-h.rain)*(float((j+1)*freq)-int((j+1)*freq)))
            tsnow=h.snow+((eplus.snow-h.snow)*(float((j+1)*freq)-int((j+1)*freq)))
            tthunderstorm=h.thunderstorm+((eplus.thunderstorm-h.thunderstorm)*(float((j+1)*freq)-int((j+1)*freq)))
            ti_f=h.ice_formation+((eplus.ice_formation-h.ice_formation)*(float((j+1)*freq)-int((j+1)*freq)))
            tm = Measurement(id=tid,station_id=tstation_id,measurement_date=tmeasurement_date,measurement_hour=tmeasurement_hour,wind_direction=twind_direction,average_wind_speed=ta_w_s,temperature=ttemperature,duration_of_sunshine=td_o_s,global_radiation=tg_r,duration_of_precipitation=td_o_p,air_pressure=ta_p,horizontal_view=th_v,overcast=tovercast,humidity=thumidity,fog=tfog,rain=train,snow=tsnow,thunderstorm=tthunderstorm,ice_formation=ti_f)
            finlist.append(tm)
            if(h.measurement_hour+1>=int((j+1)*freq)):
                h = eplus
    if(h.measurement_hour>2):
        finlist.append(Measurement.objects.get(id=h.id))
    testser = serializers.serialize('json',finlist)
    struct = json.loads(testser)
    testser = json.dumps(struct)
    querysetfin = Measurement.objects.filter(id__in=data)
    datafin = list(querysetfin.values())
    stest=1
    s = "[{\"rain\": "+str(stest)+",\"fog\": "+str(stest)+"}]"
    return HttpResponse(testser)
#priestorova interpolacia z n najblizsich stanic
def multiple(request):
    time_from = request.GET["time_from"]
    time_to = request.GET["time_to"]
    lat = float(request.GET["latitude"])
    longi = float(request.GET["longitude"])
    number_of_stations = int(request.GET["number_of_stations"])
    list_id = []
    list_dist = []
    finlist = []
#vzdialenost stanic od suradnic
    for e in MeteoStations.objects.all():
        list_id.append(e.id)
        pom = sqrt((e.latitude - lat)*(e.latitude - lat) + (e.longitude - longi)*(e.longitude - longi))
        list_dist.append(pom)
    for i in range(0,len(list_id)):
        for j in range(0,len(list_id)):
            if list_dist[i] < list_dist[j]:
                tempa = list_dist[i]
                temp = list_id[i]
                list_id[i] = list_id[j]
                list_dist[i] = list_dist[j]
                list_id[j] = temp
                list_dist[j] = tempa
    k=0
    for i in range(0,number_of_stations):
        k = k + list_dist[i]
    l = 0
    m = 0
    twind_direction = 0
    ta_w_s = 0
    ttemperature = 0
    td_o_s = 0
    tg_r = 0
    td_o_p = 0
    ta_p = 0
    th_v = 0
    tovercast = 0
    thumidity = 0
    tfog = 0
    train = 0
    tsnow = 0
    tthunderstorm = 0
    ti_f = 0
    ll = []
    last_list = []
    lomene = []
    bolo = []
    for i in range(0,number_of_stations):
        l= l + float(1/(1+list_dist[i]))
    for i in range(0,15):
        ll.append(float(l))
        bolo.append(0)
#dopocitavanie hodnot z dat z n najblizsich stanic
    queryss = Measurement.objects.filter(station_id=list_id[0],measurement_date__gte=time_from,measurement_date__lte=time_to)
    for e in queryss:
        twind_direction = 0
        ta_w_s = 0
        ttemperature = 0
        td_o_s = 0
        tg_r = 0
        td_o_p = 0
        ta_p = 0
        th_v = 0
        tovercast = 0
        thumidity = 0
        tfog = 0
        train = 0
        tsnow = 0
        tthunderstorm = 0
        ti_f = 0
        for i in range(0,number_of_stations):
            eplus= Measurement.objects.get(station_id=list_id[i],measurement_date=e.measurement_date,measurement_hour=e.measurement_hour)
            tid = e.id
            tstation_id=e.station_id
            tmeasurement_date=e.measurement_date
            tmeasurement_hour=e.measurement_hour
            if eplus.wind_direction is not None:
                twind_direction += float((1/(1+list_dist[i]))*eplus.wind_direction)
            else:
                if bolo[0]==0:
                    ll[0]-=float(1/(1+list_dist[i]))
                    bolo[0]=1
            if eplus.average_wind_speed is not None:
                ta_w_s += float((1/(1+list_dist[i]))*eplus.average_wind_speed)
            else:
                if bolo[1]==0:
                    ll[1]-=float(1/(1+list_dist[i]))
                    bolo[1]=1
            if eplus.temperature is not None:
                ttemperature += float((1/(1+list_dist[i]))*eplus.temperature)
            else:
                if bolo[2]==0:
                    ll[2]-=float(1/(1+list_dist[i]))
                    bolo[2]=1
            if eplus.duration_of_sunshine is not None:
                td_o_s += float((1/(1+list_dist[i]))*eplus.duration_of_sunshine) 
            else:
                if bolo[3]==0:
                    ll[3]-=float(1/(1+list_dist[i]))
                    bolo[3]=1
            if eplus.global_radiation is not None:
                tg_r += float((1/(1+list_dist[i]))*eplus.global_radiation)
            else:
                if bolo[4]==0:
                    ll[4]-=float(1/(1+list_dist[i]))
                    bolo[4]=1
            if eplus.duration_of_precipitation is not None:
                td_o_p += float((1/(1+list_dist[i]))*eplus.duration_of_precipitation)
            else:
                if bolo[5]==0:
                    ll[5]-=float(1/(1+list_dist[i]))
                    bolo[5]=1
            if eplus.air_pressure is not None:
                ta_p += float((1/(1+list_dist[i]))*eplus.air_pressure)
            else:
                if bolo[6]==0:
                    ll[6]-=float(1/(1+list_dist[i]))
                    bolo[6]=1
            if eplus.horizontal_view is not None:
                th_v += float((1/(1+list_dist[i]))*eplus.horizontal_view)
            else:
                if bolo[7]==0:
                    ll[7]-=float(1/(1+list_dist[i]))
                    bolo[7]=1
            if eplus.overcast is not None:
                tovercast += float((1/(1+list_dist[i]))*eplus.overcast)
            else:
                if bolo[8]==0:
                    ll[8]-=float(1/(1+list_dist[i]))
                    bolo[8]=1
            if eplus.humidity is not None:
                thumidity += float((1/(1+list_dist[i]))*eplus.humidity)
            else:
                if bolo[9]==0:
                    ll[9]-=float(1/(1+list_dist[i]))
                    bolo[9]=1
            if eplus.fog is not None:
                tfog += float((1/(1+list_dist[i]))*eplus.fog)
            else:
                if bolo[10]==0:
                    ll[10]-=float(1/(1+list_dist[i]))
                    bolo[10]=1
            if eplus.rain is not None:
                train += float((1/(1+list_dist[i]))*eplus.rain)
            else:
                if bolo[11]==0:
                    ll[11]-=float(1/(1+list_dist[i]))
                    bolo[11]=1
            if eplus.snow is not None:
                tsnow += float((1/(1+list_dist[i]))*eplus.snow)
            else:
                if bolo[12]==0:
                    ll[12]-=float(1/(1+list_dist[i]))
                    bolo[12]=1
            if eplus.thunderstorm is not None:
                tthunderstorm += float((1/(1+list_dist[i]))*eplus.thunderstorm)
            else:
                if bolo[13]==0:
                    ll[13]-=float(1/(1+list_dist[i]))
                    bolo[13]=1
            if eplus.ice_formation is not None:
                ti_f += float((1/(1+list_dist[i]))*eplus.ice_formation)
            else:
                if bolo[14]==0:
                    ll[14]-=float(1/(1+list_dist[i]))
                    bolo[14]=1
        twind_direction = float(twind_direction/ll[0])
        ta_w_s = float(ta_w_s/ll[1])
        ttemperature = float(ttemperature/ll[2])
        td_o_s = float(td_o_s/ll[3])
        tg_r = float(tg_r/ll[4])
        td_o_p = float(td_o_p/ll[5])
        ta_p = float(ta_p/ll[6])
        th_v = float(th_v/ll[7])
        tovercast = float(tovercast/ll[8])
        thumidity = float(thumidity/ll[9])
        tfog = float(tfog/ll[10])
        train = float(train/ll[11])
        tsnow = float(tsnow/ll[12])
        tthunderstorm = float(tthunderstorm/ll[13])
        ti_f = float(ti_f/ll[14])
        tm = Measurement(id=tid,station_id=tstation_id,measurement_date=tmeasurement_date,measurement_hour=tmeasurement_hour,wind_direction=twind_direction,average_wind_speed=ta_w_s,temperature=ttemperature,duration_of_sunshine=td_o_s,global_radiation=tg_r,duration_of_precipitation=td_o_p,air_pressure=ta_p,horizontal_view=th_v,overcast=tovercast,humidity=thumidity,fog=tfog,rain=train,snow=tsnow,thunderstorm=tthunderstorm,ice_formation=ti_f)
        finlist.append(tm)
    testser = serializers.serialize('json',finlist)
    struct = json.loads(testser)
    testser = json.dumps(struct)
    return HttpResponse(testser)
#dopocitavanie dat pomocou metody simple kriging
def simpleKrig(request):
    number_of_stations = int(request.GET["number_of_stations"])
    time_from = request.GET["time_from"]
    time_to = request.GET["time_to"]
    train_from = request.GET["train_from"]
    train_to = request.GET["train_to"]
    id = request.GET["id"]
    if request.method == 'GET' and 'stlpec' in request.GET:
        stlpec=int(request.GET["stlpec"])
        if stlpec > 10:
            stlpec = 3
    else:
        stlpec=3
    priemer=0
    pocall=0
    list_id = []
    list_dist = []
    stanica = MeteoStations.objects.get(id=id)
    stan = MeteoStations.objects.all()
    #vzdialenost stanic od suradnic
    for e in MeteoStations.objects.all():
        list_id.append(e.id)
        pom = sqrt((e.latitude - stanica.latitude)*(e.latitude - stanica.latitude) + (e.longitude - stanica.longitude)*(e.longitude - stanica.longitude))
        list_dist.append(pom)
    for i in range(0, len(list_id)):
        for j in range(0, len(list_id)-i-1):
            if list_dist[j] > list_dist[j + 1]:
                list_dist[j], list_dist[j + 1] = list_dist[j + 1], list_dist[j]
                list_id[j], list_id[j + 1] = list_id[j + 1], list_id[j]
    querrs = Measurement.objects.filter(measurement_date__gte=train_from, measurement_date__lte=train_to,station_id=list_id[1])
    for i in range(2,number_of_stations+1):
        querr=Measurement.objects.filter(measurement_date__gte=train_from, measurement_date__lte=train_to,station_id=list_id[i])
        querrs=querrs | querr
#data su dane do matice
    poc=0
    xi=[]
    matrixa=[]
    xa=[]
    matrix=[]
    x2i = []
    matrixa2=[]
    xa2=[]
    matrix2=[]
    cisStlpca=0
    for ee in range(0,number_of_stations):
        query = Measurement.objects.filter(measurement_date__gte=train_from, measurement_date__lte=train_to,station_id=list_id[ee])
        xi.clear()
        if list_id[ee]==id:
            cisStlpca=poc
        else:
            poc+=1

        for eee in query:
            if stlpec == 1:
                xi.append(eee.wind_direction)
                priemer += eee.wind_direction
                pocall += 1
            elif stlpec == 2:
                xi.append(eee.average_wind_speed)
                priemer += eee.average_wind_speed
                pocall += 1
            elif stlpec == 3:
                xi.append(eee.temperature)
                priemer += eee.temperature
                pocall += 1
            elif stlpec == 4:
                xi.append(eee.duration_of_sunshine)
                priemer += eee.duration_of_sunshine
                pocall += 1
            elif stlpec == 5:
                xi.append(eee.global_radiation)
                priemer += eee.global_radiation
                pocall += 1
            elif stlpec == 6:
                xi.append(eee.duration_of_precipitation)
                priemer += eee.duration_of_precipitation
                pocall += 1
            elif stlpec == 7:
                xi.append(eee.air_pressure)
                priemer += eee.air_pressure
                pocall += 1
            elif stlpec == 8:
                xi.append(eee.horizontal_view)
                priemer += eee.horizontal_view
                pocall += 1
            elif stlpec == 9:
                xi.append(eee.overcast)
                priemer += eee.overcast
                pocall += 1
            elif stlpec == 10:
                xi.append(eee.humidity)
                priemer += eee.humidity
                pocall += 1
            elif stlpec == 11:
                xi.append(eee.fog)
                priemer += eee.fog
                pocall += 1
            elif stlpec == 12:
                xi.append(eee.rain)
                priemer += eee.rain
                pocall += 1
            elif stlpec == 13:
                xi.append(eee.snow)
                priemer += eee.snow
                pocall += 1
            elif stlpec == 14:
                xi.append(eee.thunderstorm)
                priemer += eee.thunderstorm
                pocall += 1
            elif stlpec == 15:
                xi.append(eee.ice_formation)
                priemer+=eee.ice_formation
                pocall+=1
        matrixa.append(xi.copy())

    lenxi=len(xi)
    lenmatrixa=len(matrixa)
    for i in range(0,len(xi)):
        for j in range(0,len(matrixa)):
            xa.append(matrixa[j][i])
        matrix.append(xa.copy())
        xa.clear()

    for ee in range(0,number_of_stations):
        query2 = Measurement.objects.filter(measurement_date__gte=time_from, measurement_date__lte=time_to,
                                           station_id=list_id[ee])
        x2i.clear()


        for eee in query2:
            if stlpec == 1:
                x2i.append(eee.wind_direction)
                priemer += eee.wind_direction
                pocall += 1
            elif stlpec == 2:
                x2i.append(eee.average_wind_speed)
                priemer += eee.average_wind_speed
                pocall += 1
            elif stlpec == 3:
                x2i.append(eee.temperature)
                priemer += eee.temperature
                pocall += 1
            elif stlpec == 4:
                x2i.append(eee.duration_of_sunshine)
                priemer += eee.duration_of_sunshine
                pocall += 1
            elif stlpec == 5:
                x2i.append(eee.global_radiation)
                priemer += eee.global_radiation
                pocall += 1
            elif stlpec == 6:
                x2i.append(eee.duration_of_precipitation)
                priemer += eee.duration_of_precipitation
                pocall += 1
            elif stlpec == 7:
                x2i.append(eee.air_pressure)
                priemer += eee.air_pressure
                pocall += 1
            elif stlpec == 8:
                x2i.append(eee.horizontal_view)
                priemer += eee.horizontal_view
                pocall += 1
            elif stlpec == 9:
                x2i.append(eee.overcast)
                priemer += eee.overcast
                pocall += 1
            elif stlpec == 10:
                x2i.append(eee.humidity)
                priemer += eee.humidity
                pocall += 1
            elif stlpec == 11:
                x2i.append(eee.fog)
                priemer += eee.fog
                pocall += 1
            elif stlpec == 12:
                x2i.append(eee.rain)
                priemer += eee.rain
                pocall += 1
            elif stlpec == 13:
                x2i.append(eee.snow)
                priemer += eee.snow
                pocall += 1
            elif stlpec == 14:
                x2i.append(eee.thunderstorm)
                priemer += eee.thunderstorm
                pocall += 1
            elif stlpec == 15:
                x2i.append(eee.ice_formation)
                priemer+=eee.ice_formation
                pocall+=1
        matrixa2.append(x2i.copy())

    for i in range(0, len(x2i)):
        for j in range(0, len(matrixa2)):
            xa2.append(matrixa2[j][i])
        matrix2.append(xa2.copy())
        xa2.clear()
    priemer=priemer/pocall
    #e
    e=[]
    for i in range(0,len(matrix)):
        for j in range(0,len(matrix[0])):
            if i==0:
                e.append(matrix[i][j])
            else:
                e[j]=e[j]+matrix[i][j]
    for i in range(0,len(e)):
        e[i]=e[i]/len(matrix)
    #covariancna matica
    cov=[]
    cova=[]
    for i in range(0,len(matrix[0])):
        for j in range(0,len(matrix[0])):
            cova.append(0)
        cov.append(cova.copy())
        cova.clear()
    for k in range(0,len(matrix[0])):
        for l in range(0,len(matrix[0])):
            for i in range(0,len(matrix)):
                cov[k][l]+=((matrix[i][k]-e[k])*(matrix[i][l]-e[l]))
            cov[k][l]=cov[k][l]/(len(matrix)-1)
    #b
    b=[]
    ba=[]
    for i in range(0,len(matrix[0])):
        for j in range(0,len(matrix[0])):
            ba.append(0)
        b.append(ba.copy())
        ba.clear()
    for i in range(0,len(matrix[0])):
        b[i][0]=cov[i][cisStlpca]
    #cov2
    cov2=[]
    cov2a=[]
    for i in range(0,len(matrix[0])-1):
        for j in range(0,len(matrix[0])-1):
            cov2a.append(0)
        cov2.append(cov2a.copy())
        cov2a.clear()
    f=0
    for i in range(0,len(matrix[0])-1):
        if i>=cisStlpca:
            f=1
        for j in range(0,len(matrix[0])-1):
            if j>=cisStlpca:
                cov2[i][j]=cov[i+f][j+1]
            else:
                cov2[i][j] = cov[i + f][j]
    #b2
    b2=[]
    b2a=[]
    for i in range(0,len(matrix[0])-1):
        for j in range(0,len(matrix[0])-1):
            b2a.append(0)
        b2.append(b2a.copy())
        b2a.clear()
    for i in range(0,len(matrix[0])-1):
        if i>=cisStlpca:
            b2[i][0]=b[i+1][0]
        else:
            b2[i][0]=b[i+1][0]
    Ksk2=np.matrix(cov2)
    kksk2=np.matrix(b2)
    Kskinv2=np.linalg.inv(Ksk2)
    vaha2=Kskinv2*kksk2
    vysledok2=[]
    for i in range(0,len(matrix2)):
        vysledok2.append(0)
    for j in range(0,len(matrix2)):
        for i in range(0,len(matrix[0])-1):
            if i>= cisStlpca:
                vysledok2[j]+= vaha2.__getitem__((i,0))*(matrix2[j][i+1]-priemer)
            else:
                vysledok2[j] += vaha2.__getitem__((i, 0)) * (matrix2[j][i] - priemer)
        vysledok2[j]=vysledok2[j]+priemer

    testser = json.dumps(vysledok2)
    struct = json.loads(testser)
    testser = json.dumps(struct)
    return HttpResponse(testser)
#vrati atributy najblizsej stanice k suradniciam, frekvency-kolko zaznamov za 1 hodinu(tie sa dopocitaju) + pracuje aj ked data nie su konzistentne(aj Null hodnoty aj cisla)
def newdata(request):
    time_from = request.GET["time_from"]
    time_to = request.GET["time_to"]
    lat = float(request.GET["latitude"])
    longi = float(request.GET["longitude"])
    freq = float(request.GET["frequency"])
    tempa = MeteoStations.objects.get(id=210)
    temp = sqrt((tempa.latitude - lat)*(tempa.latitude - lat) + (tempa.longitude - longi)*(tempa.longitude - longi))
    tempc = 210
    queryset = MeteoStations.objects.filter(id__gte=lat,id__lte=longi)
    for e in MeteoStations.objects.all():
        a = sqrt((e.latitude - lat)*(e.latitude - lat) + (e.longitude - longi)*(e.longitude - longi))
        if a < temp:
            tempa = e
            temp = a
            tempc = e.id
    queryset = Measurement.objects.filter(station_id=tempc,measurement_date__gte=time_from,measurement_date__lte=time_to)
    secondqueryset = Measurement.objects.filter(station_id=tempc,measurement_date=time_from,measurement_hour=1)
    data = []
    finlist = []

    listdatumov =list()
    queryss = Measurement.objects.filter(station_id=tempc,measurement_date__gte=time_from,measurement_date__lte=time_to,measurement_hour=1)

    queryss = Measurement.objects.filter(station_id=tempc,measurement_date__gte=time_from,measurement_date__lte=time_to,measurement_hour=1)
    first=1
    for e in queryss:
        #finlist.append(Measurement.objects.get(id=e.id-1))
        h=Measurement.objects.get(id=e.id-1)
        s=Measurement.objects.get(id=e.id-1)
        s.measurement_date=s.measurement_date*100+s.measurement_hour
        if first == 1:
            finlist.append(s)
            first=2
        for j in range(1, 25):
            eplus= Measurement.objects.get(id=h.id+1)
            for k in range(1, int(freq)):
                tid=h.id
                tstation_id=h.station_id
                tmeasurement_date=eplus.measurement_date*100+(j-1)+k/freq
                tmeasurement_hour=(j-1)+k/freq
                if h.wind_direction is not None:
                    if eplus.wind_direction is not None:
                        twind_direction=h.wind_direction+((eplus.wind_direction-h.wind_direction)*(k/freq))
                    else:
                        twind_direction = h.wind_direction
                else:
                    twind_direction = eplus.wind_direction
                if h.average_wind_speed is not None:
                    if eplus.average_wind_speed is not None:
                        ta_w_s=h.average_wind_speed+((eplus.average_wind_speed-h.average_wind_speed)*(k/freq))
                    else:
                        ta_w_s = h.average_wind_speed
                else:
                    ta_w_s = eplus.average_wind_speed
                if h.temperature is not None:
                    if eplus.temperature is not None:
                        ttemperature=h.temperature+((eplus.temperature-h.temperature)*(k/freq))
                    else:
                        ttemperature=h.temperature
                else:
                    ttemperature = eplus.temperature
                if h.duration_of_sunshine is not None:
                    if eplus.duration_of_sunshine is not None:
                        td_o_s=h.duration_of_sunshine+((eplus.duration_of_sunshine-h.duration_of_sunshine)*(k/freq))
                    else:
                        td_o_s = h.duration_of_sunshine
                else:
                    td_o_s = eplus.duration_of_sunshine
                if h.global_radiation is not None:
                    if eplus.global_radiation is not None:
                        tg_r=h.global_radiation+((eplus.global_radiation-h.global_radiation)*(k/freq))
                    else:
                        tg_r = h.global_radiation
                else:
                    tg_r = eplus.global_radiation
                if h.duration_of_precipitation is not None:
                    if eplus.duration_of_precipitation is not None:
                        td_o_p=h.duration_of_precipitation+((eplus.duration_of_precipitation-h.duration_of_precipitation)*(k/freq))
                    else:
                        td_o_p = h.duration_of_precipitation
                else:
                    td_o_p = eplus.duration_of_precipitation
                if h.air_pressure is not None:
                    if eplus.air_pressure is not None:
                        ta_p=h.air_pressure+((eplus.air_pressure-h.air_pressure)*(k/freq))
                    else:
                        ta_p = h.air_pressure
                else:
                    ta_p = eplus.air_pressure
                if h.horizontal_view is not None:
                    if eplus.horizontal_view is not None:
                        th_v=h.horizontal_view+((eplus.horizontal_view-h.horizontal_view)*(k/freq))
                    else:
                        th_v = h.horizontal_view
                else:
                    th_v = eplus.horizontal_view
                if h.overcast is not None:
                    if eplus.overcast is not None:
                        tovercast=h.overcast+((eplus.overcast-h.overcast)*(k/freq))
                    else:
                        tovercast = h.overcast
                else:
                    tovercast = eplus.overcast
                if h.humidity is not None:
                    if eplus.humidity is not None:
                        thumidity=h.humidity+((eplus.humidity-h.humidity)*(k/freq))
                    else:
                        thumidity = h.humidity
                else:
                    thumidity = eplus.humidity
                if h.fog is not None:
                    if eplus.fog is not None:
                        tfog=h.fog+((eplus.fog-h.fog)*(k/freq))
                    else:
                        tfog = h.fog
                else:
                    tfog = eplus.fog
                if h.rain is not None:
                    if eplus.rain is not None:
                        train=h.rain+((eplus.rain-h.rain)*(k/freq))
                    else:
                        train = h.rain
                else:
                    train = eplus.rain
                if h.snow is not None:
                    if eplus.snow is not None:
                        tsnow=h.snow+((eplus.snow-h.snow)*(k/freq))
                    else:
                        tsnow = h.snow
                else:
                    tsnow = eplus.snow
                if h.thunderstorm is not None:
                    if eplus.thunderstorm is not None:
                        tthunderstorm=h.thunderstorm+((eplus.thunderstorm-h.thunderstorm)*(k/freq))
                    else:
                        tthunderstorm = h.thunderstorm
                else:
                    tthunderstorm = eplus.thunderstorm
                if h.ice_formation is not None:
                    if eplus.ice_formation is not None:
                        ti_f=h.ice_formation+((eplus.ice_formation-h.ice_formation)*(k/freq))
                    else:
                        ti_f = h.ice_formation
                else:
                    ti_f = eplus.ice_formation
                tm = Measurement(id=tid,station_id=tstation_id,measurement_date=tmeasurement_date,measurement_hour=tmeasurement_hour,wind_direction=twind_direction,average_wind_speed=ta_w_s,temperature=ttemperature,duration_of_sunshine=td_o_s,global_radiation=tg_r,duration_of_precipitation=td_o_p,air_pressure=ta_p,horizontal_view=th_v,overcast=tovercast,humidity=thumidity,fog=tfog,rain=train,snow=tsnow,thunderstorm=tthunderstorm,ice_formation=ti_f)
                finlist.append(tm)
            h = eplus
            hu=Measurement.objects.get(id=h.id)
            hu.measurement_date=hu.measurement_date*100+hu.measurement_hour
            finlist.append(hu)
    testser = serializers.serialize('json',finlist)
    struct = json.loads(testser)
    testser = json.dumps(struct)
    querysetfin = Measurement.objects.filter(id__in=data)
    datafin = list(querysetfin.values())
    stest=1
    s = "[{\"rain\": "+str(stest)+",\"fog\": "+str(stest)+"}]"
#    return JsonResponse(listdatumov,safe=False)
    return HttpResponse(testser)
#    return HttpResponse(s)
#hlavne menu(este zatial nic)
def menu(request):
    #return HttpResponse("<head>hello</head><body><p>World</p></body>")
    return render(request,'menu.http')
def m1(request):
    return render(request,'m1.http')
def m2(request):
    return render(request,'m2.http')
def m3(request):
    return render(request,'m3.http')
def m4(request):
    return render(request,'m4.http')
def m5(request):
    return render(request,'m5.http')
def m6(request):
    return render(request,'m6.http')
def m8(request):
    return render(request,'m8.http')
def m9(request):
    return render(request,'m9.http')
def m10(request):
    return render(request,'m10.http')
def m11(request):
    return render(request,'m11.http')
def m12(request):
    return render(request,'m12.http')
def m13(request):
    return render(request,'m13.http')    
    # metoda najblizsieho suseda
def nearneigh(request):
    time_from = request.GET["time_from"]
    time_to = request.GET["time_to"]
    lat = float(request.GET["latitude"])
    longi = float(request.GET["longitude"])
    stlpec = float(request.GET["stlpec"])
    tempa = MeteoStations.objects.get(id=210)
    xi=[]
#vzdialenosti stanic od suradnic
    temp = sqrt((tempa.latitude - lat)*(tempa.latitude - lat) + (tempa.longitude - longi)*(tempa.longitude - longi))
    tempc = 210
#hladanie najblizsej stanice
    queryset = MeteoStations.objects.filter(id__gte=lat,id__lte=longi)
    for e in MeteoStations.objects.all():
        a = sqrt((e.latitude - lat)*(e.latitude - lat) + (e.longitude - longi)*(e.longitude - longi))
        if a < temp:
            tempa = e
            temp = a
            tempc = e.id
    querrs = Measurement.objects.filter(measurement_date__gte=time_from, measurement_date__lte=time_to,station_id=tempc)
    for eee in querrs:
            if stlpec == 1:
                xi.append(eee.wind_direction)
            elif stlpec == 2:
                xi.append(eee.average_wind_speed)
            elif stlpec == 3:
                xi.append(eee.temperature)
            elif stlpec == 4:
                xi.append(eee.duration_of_sunshine)
            elif stlpec == 5:
                xi.append(eee.global_radiation)
            elif stlpec == 6:
                xi.append(eee.duration_of_precipitation)
            elif stlpec == 7:
                xi.append(eee.air_pressure)
            elif stlpec == 8:
                xi.append(eee.horizontal_view)
            elif stlpec == 9:
                xi.append(eee.overcast)
            elif stlpec == 10:
                xi.append(eee.humidity)
            elif stlpec == 11:
                xi.append(eee.fog)
            elif stlpec == 12:
                xi.append(eee.rain)
            elif stlpec == 13:
                xi.append(eee.snow)
            elif stlpec == 14:
                xi.append(eee.thunderstorm)
            elif stlpec == 15:
                xi.append(eee.ice_formation)
    testser = json.dumps(xi)
    return HttpResponse(testser)
    
    
def idw(request):
    lat = float(request.GET["latitude"])
    longi = float(request.GET["longitude"])
    number_of_stations = int(request.GET["number_of_stations"])
    time_from = request.GET["time_from"]
    time_to = request.GET["time_to"]
    stlpec= int(request.GET["stlpec"])
    list_id = []
    list_dist = []
    vysledok = []
    
    #vzdialenost stanic od suradnic
    for e in MeteoStations.objects.all():
        list_id.append(e.id)
        pom = sqrt((e.latitude - lat)*(e.latitude - lat) + (e.longitude - longi)*(e.longitude - longi))
        list_dist.append(pom)
    for i in range(0, len(list_id)):
        for j in range(0, len(list_id)-i-1):
            if list_dist[j] > list_dist[j + 1]:
                list_dist[j], list_dist[j + 1] = list_dist[j + 1], list_dist[j]
                list_id[j], list_id[j + 1] = list_id[j + 1], list_id[j]

    dobrest=0
    while True:
    
        queryset = Measurement.objects.filter(station_id=list_id[dobrest],measurement_date__gte=time_from,measurement_date__lte=time_to)
        jeok=True
        for e in queryset:
            if stlpec == 1:
                if e.wind_direction is None:
                    jeok=False
            elif stlpec == 2:
                if e.average_wind_speed is None:
                    jeok=False
            elif stlpec == 3:
                if e.temperature is None:
                    jeok=False
            elif stlpec == 4:
                if e.duration_of_sunshine is None:
                    jeok=False
            elif stlpec == 5:
                if e.global_radiation is None:
                    jeok=False
            elif stlpec == 6:
                if e.duration_of_precipitation is None:
                    jeok=False
            elif stlpec == 7:
                if e.air_pressure is None:
                    jeok=False
            elif stlpec == 8:
                if e.horizontal_view is None:
                    jeok=False
            elif stlpec == 9:
                if e.overcast is None:
                    jeok=False
            elif stlpec == 10:
                if e.humidity is None:
                    jeok=False
            elif stlpec == 11:
                if e.fog is None:
                    jeok=False
            elif stlpec == 12:
                if e.rain is None:
                    jeok=False
            elif stlpec == 13:
                if e.snow is None:
                    jeok=False
            elif stlpec == 14:
                if e.thunderstorm is None:
                    jeok=False
            elif stlpec == 15:
                if e.ice_formation is None:
                    jeok=False
        if jeok:
            dobrest+=1
            
        else:
            list_id.pop(dobrest)
            list_dist.pop(dobrest)    
        if dobrest == number_of_stations:
            break                
                
    query = Measurement.objects.filter(station_id=list_id[0],measurement_date__gte=time_from,measurement_date__lte=time_to)
    delitel=0
    for i in range(0,number_of_stations):
        delitel+=float(1/list_dist[i])
    vysl=[]
    for e in query:
        if stlpec == 1:
            vysl.append(float(e.wind_direction/list_dist[i]))
        elif stlpec == 2:
            vysl.append(float(e.average_wind_speed/list_dist[i]))
        elif stlpec == 3:
            vysl.append(float(e.temperature/list_dist[i]))
        elif stlpec == 4:
            vysl.append(float(e.duration_of_sunshine/list_dist[i]))
        elif stlpec == 5:
            vysl.append(float(e.global_radiation/list_dist[i]))
        elif stlpec == 6:
            vysl.append(float(e.duration_of_precipitation/list_dist[i]))
        elif stlpec == 7:
            vysl.append(float(e.air_pressure/list_dist[i]))
        elif stlpec == 8:
            vysl.append(float(e.horizontal_view/list_dist[i]))
        elif stlpec == 9:
            vysl.append(float(e.overcast/list_dist[i]))
        elif stlpec == 10:
            vysl.append(float(e.humidity/list_dist[i]))
        elif stlpec == 11:
            vysl.append(float(e.fog/list_dist[i]))
        elif stlpec == 12:
            vysl.append(float(e.rain/list_dist[i]))
        elif stlpec == 13:
            vysl.append(float(e.snow/list_dist[i]))
        elif stlpec == 14:
            vysl.append(float(e.thunderstorm/list_dist[i]))
        elif stlpec == 15:
            vysl.append(float(e.ice_formation/list_dist[i]))
    for i in range(1,number_of_stations):
        query = Measurement.objects.filter(station_id=list_id[i],measurement_date__gte=time_from,measurement_date__lte=time_to)
        vyslpom=[]
        for e in query:
            if stlpec == 1:
                vyslpom.append(float(e.wind_direction/list_dist[i]))
            elif stlpec == 2:
                vyslpom.append(float(e.average_wind_speed/list_dist[i]))
            elif stlpec == 3:
                vyslpom.append(float(e.temperature/list_dist[i]))
            elif stlpec == 4:
                vyslpom.append(float(e.duration_of_sunshine/list_dist[i]))
            elif stlpec == 5:
                vyslpom.append(float(e.global_radiation/list_dist[i]))
            elif stlpec == 6:
                vyslpom.append(float(e.duration_of_precipitation/list_dist[i]))
            elif stlpec == 7:
                vyslpom.append(float(e.air_pressure/list_dist[i]))
            elif stlpec == 8:
                vyslpom.append(float(e.horizontal_view/list_dist[i]))
            elif stlpec == 9:
                vyslpom.append(float(e.overcast/list_dist[i]))
            elif stlpec == 10:
                vyslpom.append(float(e.humidity/list_dist[i]))
            elif stlpec == 11:
                vyslpom.append(float(e.fog/list_dist[i]))
            elif stlpec == 12:
                vyslpom.append(float(e.rain/list_dist[i]))
            elif stlpec == 13:
                vyslpom.append(float(e.snow/list_dist[i]))
            elif stlpec == 14:
                vyslpom.append(float(e.thunderstorm/list_dist[i]))
            elif stlpec == 15:
                vyslpom.append(float(e.ice_formation/list_dist[i]))
        vysl=np.sum([vysl,vyslpom], axis=0)
    """
    for e in query:
        sstlpec=0.0
                            
        for i in range(0,number_of_stations):
            eplus= Measurement.objects.get(station_id=list_id[i],measurement_date=e.measurement_date,measurement_hour=e.measurement_hour)
            if stlpec == 1:
                sstlpec+=float(eplus.wind_direction/list_dist[i])
            elif stlpec == 2:
                sstlpec+=float(eplus.average_wind_speed/list_dist[i])
            elif stlpec == 3:
                sstlpec+=float(eplus.temperature/list_dist[i])
            elif stlpec == 4:
                sstlpec+=float(eplus.duration_of_sunshine/list_dist[i])
            elif stlpec == 5:
                sstlpec+=float(eplus.global_radiation/list_dist[i])
            elif stlpec == 6:
                sstlpec+=float(eplus.duration_of_precipitation/list_dist[i])
            elif stlpec == 7:
                sstlpec+=float(eplus.air_pressure/list_dist[i])
            elif stlpec == 8:
                sstlpec+=float(eplus.horizontal_view/list_dist[i])
            elif stlpec == 9:
                sstlpec+=float(eplus.overcast/list_dist[i])
            elif stlpec == 10:
                sstlpec+=float(eplus.humidity/list_dist[i])
            elif stlpec == 11:
                sstlpec+=float(eplus.fog/list_dist[i])
            elif stlpec == 12:
                sstlpec+=float(eplus.rain/list_dist[i])
            elif stlpec == 13:
                sstlpec+=float(eplus.snow/list_dist[i])
            elif stlpec == 14:
                sstlpec+=float(eplus.thunderstorm/list_dist[i])
            elif stlpec == 15:
                sstlpec+=float(eplus.ice_formation/list_dist[i])
            sstlpec=float(sstlpec/delitel)
        vysledok.append(sstlpec)
    """
    for i in range(0,len(vysl)):
        vysl[i]=float(vysl[i]/delitel)
    ttv= vysl.tolist()
    testser = json.dumps(ttv)
    return HttpResponse(testser)
    
    
def naturneigh(request):
    lat = float(request.GET["latitude"])
    longi = float(request.GET["longitude"])
    a=[]
    for e in MeteoStations.objects.all():
        a.append([e.latitude,e.longitude])
    b=a
    b.append([lat,longi])
    from scipy.spatial import Voronoi
    from scipy.spatial import ConvexHull
    v = Voronoi(a)
    v2= Voronoi(b)
    vol2=np.zeros(v2.npoints)
    vol = np.zeros(v.npoints)
    volrozd=np.zeros(v2.npoints)
    for i, reg_num in enumerate(v.point_region):
        indices = v.regions[reg_num]
        if -1 in indices: # some regions can be opened
            vol[i] = np.inf
        else:
            vol[i] = ConvexHull(v.vertices[indices]).volume
    for i, reg_num in enumerate(v2.point_region):
        indices = v2.regions[reg_num]
        if -1 in indices: # some regions can be opened
            vol2[i] = np.inf
        else:
            vol2[i] = ConvexHull(v2.vertices[indices]).volume
    #vol.append(0)  
    
    for i in vol:
        volrozd[i]=vol[i]-vol2[i]
    
    testser=volrozd
    #testser=json.dumps(c)
    return HttpResponse(testser)
    
    
    
    
    
from scipy.spatial import Voronoi,Delaunay
import numpy as np
import matplotlib.pyplot as plt

def tetravol(a,b,c,d):
    #'''Calculates the volume of a tetrahedron, given vertices a,b,c and d (triplets)'''
    tetravol=abs(np.dot((a-d),np.cross((b-d),(c-d))))/6
    return tetravol

def vol(vor,p):
    #'''Calculate volume of 3d Voronoi cell based on point p. Voronoi diagram is passed in v.'''
    dpoints=[]
    vol=0
    for v in vor.regions[vor.point_region[p]]:
        dpoints.append(list(vor.vertices[v]))
    tri=Delaunay(np.array(dpoints))
    for simplex in tri.simplices:
        vol+=tetravol(np.array(dpoints[simplex[0]]),np.array(dpoints[simplex[1]]),np.array(dpoints[simplex[2]]),np.array(dpoints[simplex[3]]))
    return vol
def lalala(request):
    vysl=""
    a=[]
    for e in MeteoStations.objects.all():
        a.append([e.latitude,e.longitude])
    vor=Voronoi(a)
    #x= [np.random.random() for i in xrange(50)]
    #y= [np.random.random() for i in xrange(50)]
    dpoints=[]
    #points=zip(x,y)
    #vor=Voronoi(points)
    vtot=0

    for i,p in enumerate(vor.points):
        out=False
        for v in vor.regions[vor.point_region[i]]:
            if v<=-1: #a point index of -1 is returned if the vertex is outside the Vornoi diagram, in this application these should be ignorable edge-cases
                out=True
            else:
                if not out:
                    pvol=vol(vor,i)
                    vtot+=pvol
                    vysl+= "point "+str(i)+" with coordinates "+str(p)+" has volume "+str(pvol)

    vysl+= "total volume= "+str(vtot)
    return vysl
    
import folium
import pandas
def stations(request):
    m = folium.Map(location=[52.212, 5.2793],zoom_start=7)
    for e in MeteoStations.objects.all():
        folium.Marker(location=[e.latitude,e.longitude],icon=folium.Icon(color='blue', icon='umbrella', prefix='fa')).add_to(m)
    return HttpResponse(m.get_root().render())
from scipy import interpolate
def splinemap2(request):
    x_edges, y_edges = np.mgrid[-1:1:210j, -1:1:210j]
    x = x_edges[:-1, :-1] + np.diff(x_edges[:2, 0])[0] / 2.
    y = y_edges[:-1, :-1] + np.diff(y_edges[0, :2])[0] / 2.
    z = (x+y) * np.exp(-6.0*(x*x+y*y))
    plt.figure()
    lims = dict(cmap='RdBu_r', vmin=-0.25, vmax=0.25)
    plt.pcolormesh(x_edges, y_edges, z, shading='flat', **lims)
    plt.colorbar()
    plt.title("Sparsely sampled function.")
    plt.show()
    return HttpResponse("Vykreslené v novom okne")  
from scipy.interpolate import Rbf
def splinemap(request):
    date = request.GET["date"]
    hour = request.GET["hour"]
    stlpec = int(request.GET["stlpec"])
    num = int(request.GET["num"])
    lons=np.empty((0,1), float)
    lats=np.empty((0,1), float)
    data=np.empty((0,1), int)
    
    for e in MeteoStations.objects.all():
        lons=np.append(lons, np.array([[e.longitude]]), axis=0)
        lats=np.append(lats, np.array([[e.latitude]]), axis=0)
        query=Measurement.objects.get(measurement_date=date,measurement_hour=hour,station_id=e.id)
        if stlpec == 1:
            c=query.wind_direction
        elif stlpec == 2:
            c=query.average_wind_speed
        elif stlpec == 3:
            c=query.temperature
        elif stlpec == 4:
            c=query.duration_of_sunshine
        elif stlpec == 5:
            c=query.global_radiation
        elif stlpec == 6:
            c=query.duration_of_precipitation
        elif stlpec == 7:
            c=query.air_pressure
        elif stlpec == 8:
           c=query.horizontal_view
        elif stlpec == 9:
            c=querye.overcast
        elif stlpec == 10:
            c=query.humidity
        elif stlpec == 11:
            c=query.fog
        elif stlpec == 12:
            c=query.rain
        elif stlpec == 13:
            c=query.snow
        elif stlpec == 14:
            c=query.thunderstorm
        elif stlpec == 15:
            c=query.ice_formation
        if c is not None:
            lons=np.append(lons, np.array([[e.longitude]]), axis=0)
            lats=np.append(lats, np.array([[e.latitude]]), axis=0)
            data=np.append(data, np.array([[c]]), axis=0)
    edges = np.linspace(0.0, 60.0, num)
    centers = edges[:-1] + np.diff(edges[:2])[0] / 2.

    XI, YI = np.meshgrid(centers, centers)
    #rbf = Rbf(lats, lons, data, epsilon=2)
    rbf = Rbf(lons, lats, data)
    ZI = rbf(XI, YI)
    plt.subplot(1, 1, 1)

    X_edges, Y_edges = np.meshgrid(edges, edges)
    lims = dict(cmap='Spectral_r', vmin=np.amin(data), vmax=np.amax(data))
    plt.pcolormesh(X_edges, Y_edges, ZI, shading='flat', **lims)
    #plt.scatter(lats, lons, 100, data, edgecolor='w', lw=0.1, **lims)
    #plt.scatter(lons, lats, 100, data, edgecolor='w', lw=0.1, **lims)
    plt.title('RBF interpolation')
    #plt.xlim(np.amin(lats), np.amax(lats))
    #plt.ylim(np.amin(lons), np.amax(lons))
    plt.ylim(np.amin(lats), np.amax(lats))
    plt.xlim(np.amin(lons), np.amax(lons))
    plt.colorbar()
    plt.show()
    return HttpResponse("Vykreslené v novom okne")  
from scipy.spatial import Voronoi, voronoi_plot_2d
import matplotlib.pyplot as plt
def voronoi(request):
    date = request.GET["date"]
    hour = request.GET["hour"]
    stlpec = int(request.GET["stlpec"])
    a=np.empty((0,2), float)
    b=np.empty((0,1), int)
    for e in MeteoStations.objects.all():
        #a=np.append(a,[e.latitude,e.longitude])
        try:
            query=Measurement.objects.get(measurement_date=date,measurement_hour=hour,station_id=e.id)
        except query.DoesNotExist:
            pomocna=1
        if stlpec == 1:
            c=query.wind_direction
        elif stlpec == 2:
            c=query.average_wind_speed
        elif stlpec == 3:
            c=query.temperature
        elif stlpec == 4:
            c=query.duration_of_sunshine
        elif stlpec == 5:
            c=query.global_radiation
        elif stlpec == 6:
            c=query.duration_of_precipitation
        elif stlpec == 7:
            c=query.air_pressure
        elif stlpec == 8:
            c=query.horizontal_view
        elif stlpec == 9:
            c=querye.overcast
        elif stlpec == 10:
            c=query.humidity
        elif stlpec == 11:
            c=query.fog
        elif stlpec == 12:
            c=query.rain
        elif stlpec == 13:
            c=query.snow
        elif stlpec == 14:
            c=query.thunderstorm
        elif stlpec == 15:
            c=query.ice_formation
        if c is not None:
            a=np.append(a, np.array([[e.latitude,e.longitude]]), axis=0)
            b=np.append(b, np.array([[c]]), axis=0)
    #query = Measurement.objects.filter(measurement_date=date,measurement_hour=hour)
    #for e in query:
        #b.np.append(b,np.array([e.temperature]), axis=0)
    
    minima = min(b)
    maxima = max(b)

    norm = mpl.colors.Normalize(vmin=minima, vmax=maxima, clip=True)
    #mapper = cm.ScalarMappable(norm=norm, cmap=cm.Blues_r)
    mapper = cm.ScalarMappable(norm=norm, cmap=cm.Spectral_r)
            
    vor=Voronoi(a)
    voronoi_plot_2d(vor, show_points=True, show_vertices=False, s=1)
    for r in range(len(vor.point_region)):
        region = vor.regions[vor.point_region[r]]
        if not -1 in region:
            polygon = [vor.vertices[i] for i in region]
            plt.fill(*zip(*polygon), color=mapper.to_rgba(b[r]),label='data')
    hid = plt.pcolormesh(b,norm=norm, antialiased=True,cmap='Spectral_r')
    plt.colorbar()
    #plt.legend(['stanice','ciary','das','ads','adsa','adsb','adsc'])    
    plt.show()
    #fig = voronoi_plot_2d(vor)
    #plt.show()
    return HttpResponse("Vykreslené v novom okne")
    
def linreg(request):
    time_from = request.GET["time_from"]
    time_to = request.GET["time_to"]
    stlpecx= int(request.GET["stlpecx"])
    stlpecy= int(request.GET["stlpecy"])
    
    #x=np.empty((0,1),float)
    x=np.array([])
    #y=np.empty((0,1),float)
    y=np.array([])
    
    query= Measurement.objects.filter(measurement_date__gte=time_from,measurement_date__lte=time_to)
    for e in query:
    	jenul=0
    	if stlpecx == 1 and e.wind_direction is None:
            jenul=1
    	elif stlpecx == 2 and e.average_wind_speed is None:
            jenul=1
    	elif stlpecx == 3 and e.temperature is None:
            jenul=1
    	elif stlpecx == 4 and e.duration_of_sunshine is None:
            jenul=1
    	elif stlpecx == 5 and e.global_radiation is None:
            jenul=1
    	elif stlpecx == 6 and e.duration_of_precipitation is None:
            jenul=1
    	elif stlpecx == 7 and e.air_pressure is None:
            jenul=1
    	elif stlpecx == 8 and e.horizontal_view is None:
            jenul=1
    	elif stlpecx == 9 and e.overcast is None:
            jenul=1
    	elif stlpecx == 10 and e.humidity is None:
            jenul=1
    	elif stlpecx == 11 and e.fog is None:
            jenul=1
    	elif stlpecx == 12 and e.rain is None:
            jenul=1
    	elif stlpecx == 13 and e.snow is None:
            jenul=1
    	elif stlpecx == 14 and e.thunderstorm is None:
            jenul=1
    	elif stlpecx == 15 and e.ice_formation is None:
            jenul=1
        
    	if stlpecy == 1 and e.wind_direction is None:
            jenul=1
    	elif stlpecy == 2 and e.average_wind_speed is None:
            jenul=1
    	elif stlpecy == 3 and e.temperature is None:
            jenul=1
    	elif stlpecy == 4 and e.duration_of_sunshine is None:
            jenul=1
    	elif stlpecy == 5 and e.global_radiation is None:
            jenul=1
    	elif stlpecy == 6 and e.duration_of_precipitation is None:
            jenul=1
    	elif stlpecy == 7 and e.air_pressure is None:
            jenul=1
    	elif stlpecy == 8 and e.horizontal_view is None:
            jenul=1
    	elif stlpecy == 9 and e.overcast is None:
            jenul=1
    	elif stlpecy == 10 and e.humidity is None:
            jenul=1
    	elif stlpecy == 11 and e.fog is None:
            jenul=1
    	elif stlpecy == 12 and e.rain is None:
            jenul=1
    	elif stlpecy == 13 and e.snow is None:
            jenul=1
    	elif stlpecy == 14 and e.thunderstorm is None:
            jenul=1
    	elif stlpecy == 15 and e.ice_formation is None:
            jenul=1
             
    	if jenul == 0:
            if stlpecx == 1:
                x=np.append(x, np.array([e.wind_direction]), axis=0)
            elif stlpecx == 2:
                x=np.append(x, np.array([e.average_wind_speed]), axis=0)
            elif stlpecx == 3:
                x=np.append(x, np.array([e.temperature]), axis=0)
            elif stlpecx == 4:
                x=np.append(x, np.array([e.duration_of_sunshine]), axis=0)
            elif stlpecx == 5:
                x=np.append(x, np.array([e.global_radiation]), axis=0)
            elif stlpecx == 6:
                x=np.append(x, np.array([e.duration_of_precipitation]), axis=0)
            elif stlpecx == 7:
                x=np.append(x, np.array([e.air_pressure]), axis=0)
            elif stlpecx == 8:
                x=np.append(x, np.array([e.horizontal_view]), axis=0)
            elif stlpecx == 9:
                x=np.append(x, np.array([e.overcast]), axis=0)
            elif stlpecx == 10:
                x=np.append(x, np.array([e.humidity]), axis=0)
            elif stlpecx == 11:
                x=np.append(x, np.array([e.fog]), axis=0)
            elif stlpecx == 12:
                x=np.append(x, np.array([e.rain]), axis=0)
            elif stlpecx == 13:
                x=np.append(x, np.array([e.snow]), axis=0)
            elif stlpecx == 14:
                x=np.append(x, np.array([e.thunderstorm]), axis=0)
            elif stlpecx == 15:
                x=np.append(x, np.array([e.ice_formation]), axis=0)
            
            if stlpecy == 1:
                y=np.append(y, np.array([e.wind_direction]), axis=0)
            elif stlpecy == 2:
                y=np.append(y, np.array([e.average_wind_speed]), axis=0)
            elif stlpecy == 3:
                y=np.append(y, np.array([e.temperature]), axis=0)
            elif stlpecy == 4:
                y=np.append(y, np.array([e.duration_of_sunshine]), axis=0)
            elif stlpecy == 5:
                y=np.append(y, np.array([e.global_radiation]), axis=0)
            elif stlpecy == 6:
                y=np.append(y, np.array([e.duration_of_precipitation]), axis=0)
            elif stlpecy == 7:
                y=np.append(y, np.array([e.air_pressure]), axis=0)
            elif stlpecy == 8:
                y=np.append(y, np.array([e.horizontal_view]), axis=0)
            elif stlpecy == 9:
                y=np.append(y, np.array([e.overcast]), axis=0)
            elif stlpecy == 10:
                y=np.append(y, np.array([e.humidity]), axis=0)
            elif stlpecy == 11:
                y=np.append(y, np.array([e.fog]), axis=0)
            elif stlpecy == 12:
                y=np.append(y, np.array([e.rain]), axis=0)
            elif stlpecy == 13:
                y=np.append(y, np.array([e.snow]), axis=0)
            elif stlpecy == 14:
                y=np.append(y, np.array([e.thunderstorm]), axis=0)
            elif stlpecy == 15:
                y=np.append(y, np.array([e.ice_formation]), axis=0)
    
    if stlpecx == 1:
        xn="wind direction"
    elif stlpecx == 2:
        xn="average wind speed"
    elif stlpecx == 3:
        xn="temperature"
    elif stlpecx == 4:
        xn="duration of sunshine"
    elif stlpecx == 5:
        xn="global radiation"
    elif stlpecx == 6:
        xn="duration of precipitation"
    elif stlpecx == 7:
        xn="air pressure"
    elif stlpecx == 8:
        xn="horizontal view"
    elif stlpecx == 9:
        xn="overcast"
    elif stlpecx == 10:
        xn="hunidity"
    elif stlpecx == 11:
        xn="fog"
    elif stlpecx == 12:
        xn="rain"
    elif stlpecx == 13:
        xn="snow"
    elif stlpecx == 14:
        xn="thunderstorm"
    elif stlpecx == 15:
        xn="ice formation"
    
    if stlpecy == 1:
        yn="wind direction"
    elif stlpecy == 2:
        yn="average wind speed"
    elif stlpecy == 3:
        yn="temperature"
    elif stlpecy == 4:
        yn="duration of sunshine"
    elif stlpecy == 5:
        yn="global radiation"
    elif stlpecy == 6:
        yn="duration of precipitation"
    elif stlpecy == 7:
        yn="air pressure"
    elif stlpecy == 8:
        yn="horizontal view"
    elif stlpecy == 9:
        yn="overcast"
    elif stlpecy == 10:
        yn="hunidity"
    elif stlpecy == 11:
        yn="fog"
    elif stlpecy == 12:
        yn="rain"
    elif stlpecy == 13:
        yn="snow"
    elif stlpecy == 14:
        yn="thunderstorm"
    elif stlpecy == 15:
        yn="ice formation"
    
            
    n=np.size(x)
    m_x=np.mean(x)
    m_y=np.mean(y)
    
    SS_xy = np.sum(y*x) - n*m_y*m_x
    SS_xx = np.sum(x*x) - n*m_x*m_x
    
    b_1 = SS_xy / SS_xx
    b_0 = m_y - b_1*m_x
    
    plt.scatter(x, y, color = "m", marker = "o", s = 30)
    
    y_pred = b_0 + b_1*x
    
    plt.plot(x, y_pred, color = "g")
    tita="y= b0 + b1 * x, b0 = "
    titb=", b1 = "
    tit=tita+str(b_0)+titb+str(b_1)
    plt.title(tit)
    plt.xlabel(xn)
    plt.ylabel(yn)
    
    plt.show()
    return HttpResponse("Vykreslené v novom okne")
    
def polyreg(request):
    time_from = request.GET["time_from"]
    time_to = request.GET["time_to"]
    stlpecx= int(request.GET["stlpecx"])
    stlpecy= int(request.GET["stlpecy"])
    stupen= int(request.GET["stupen"])
    
    #x=np.empty((0,1),float)
    x=np.array([])
    #y=np.empty((0,1),float)
    y=np.array([])
    
    query= Measurement.objects.filter(measurement_date__gte=time_from,measurement_date__lte=time_to)
    for e in query:
    	jenul=0
    	if stlpecx == 1 and e.wind_direction is None:
            jenul=1
    	elif stlpecx == 2 and e.average_wind_speed is None:
            jenul=1
    	elif stlpecx == 3 and e.temperature is None:
            jenul=1
    	elif stlpecx == 4 and e.duration_of_sunshine is None:
            jenul=1
    	elif stlpecx == 5 and e.global_radiation is None:
            jenul=1
    	elif stlpecx == 6 and e.duration_of_precipitation is None:
            jenul=1
    	elif stlpecx == 7 and e.air_pressure is None:
            jenul=1
    	elif stlpecx == 8 and e.horizontal_view is None:
            jenul=1
    	elif stlpecx == 9 and e.overcast is None:
            jenul=1
    	elif stlpecx == 10 and e.humidity is None:
            jenul=1
    	elif stlpecx == 11 and e.fog is None:
            jenul=1
    	elif stlpecx == 12 and e.rain is None:
            jenul=1
    	elif stlpecx == 13 and e.snow is None:
            jenul=1
    	elif stlpecx == 14 and e.thunderstorm is None:
            jenul=1
    	elif stlpecx == 15 and e.ice_formation is None:
            jenul=1
        
    	if stlpecy == 1 and e.wind_direction is None:
            jenul=1
    	elif stlpecy == 2 and e.average_wind_speed is None:
            jenul=1
    	elif stlpecy == 3 and e.temperature is None:
            jenul=1
    	elif stlpecy == 4 and e.duration_of_sunshine is None:
            jenul=1
    	elif stlpecy == 5 and e.global_radiation is None:
            jenul=1
    	elif stlpecy == 6 and e.duration_of_precipitation is None:
            jenul=1
    	elif stlpecy == 7 and e.air_pressure is None:
            jenul=1
    	elif stlpecy == 8 and e.horizontal_view is None:
            jenul=1
    	elif stlpecy == 9 and e.overcast is None:
            jenul=1
    	elif stlpecy == 10 and e.humidity is None:
            jenul=1
    	elif stlpecy == 11 and e.fog is None:
            jenul=1
    	elif stlpecy == 12 and e.rain is None:
            jenul=1
    	elif stlpecy == 13 and e.snow is None:
            jenul=1
    	elif stlpecy == 14 and e.thunderstorm is None:
            jenul=1
    	elif stlpecy == 15 and e.ice_formation is None:
            jenul=1
             
    	if jenul == 0:
            if stlpecx == 1:
                x=np.append(x, np.array([e.wind_direction]), axis=0)
            elif stlpecx == 2:
                x=np.append(x, np.array([e.average_wind_speed]), axis=0)
            elif stlpecx == 3:
                x=np.append(x, np.array([e.temperature]), axis=0)
            elif stlpecx == 4:
                x=np.append(x, np.array([e.duration_of_sunshine]), axis=0)
            elif stlpecx == 5:
                x=np.append(x, np.array([e.global_radiation]), axis=0)
            elif stlpecx == 6:
                x=np.append(x, np.array([e.duration_of_precipitation]), axis=0)
            elif stlpecx == 7:
                x=np.append(x, np.array([e.air_pressure]), axis=0)
            elif stlpecx == 8:
                x=np.append(x, np.array([e.horizontal_view]), axis=0)
            elif stlpecx == 9:
                x=np.append(x, np.array([e.overcast]), axis=0)
            elif stlpecx == 10:
                x=np.append(x, np.array([e.humidity]), axis=0)
            elif stlpecx == 11:
                x=np.append(x, np.array([e.fog]), axis=0)
            elif stlpecx == 12:
                x=np.append(x, np.array([e.rain]), axis=0)
            elif stlpecx == 13:
                x=np.append(x, np.array([e.snow]), axis=0)
            elif stlpecx == 14:
                x=np.append(x, np.array([e.thunderstorm]), axis=0)
            elif stlpecx == 15:
                x=np.append(x, np.array([e.ice_formation]), axis=0)
            
            if stlpecy == 1:
                y=np.append(y, np.array([e.wind_direction]), axis=0)
            elif stlpecy == 2:
                y=np.append(y, np.array([e.average_wind_speed]), axis=0)
            elif stlpecy == 3:
                y=np.append(y, np.array([e.temperature]), axis=0)
            elif stlpecy == 4:
                y=np.append(y, np.array([e.duration_of_sunshine]), axis=0)
            elif stlpecy == 5:
                y=np.append(y, np.array([e.global_radiation]), axis=0)
            elif stlpecy == 6:
                y=np.append(y, np.array([e.duration_of_precipitation]), axis=0)
            elif stlpecy == 7:
                y=np.append(y, np.array([e.air_pressure]), axis=0)
            elif stlpecy == 8:
                y=np.append(y, np.array([e.horizontal_view]), axis=0)
            elif stlpecy == 9:
                y=np.append(y, np.array([e.overcast]), axis=0)
            elif stlpecy == 10:
                y=np.append(y, np.array([e.humidity]), axis=0)
            elif stlpecy == 11:
                y=np.append(y, np.array([e.fog]), axis=0)
            elif stlpecy == 12:
                y=np.append(y, np.array([e.rain]), axis=0)
            elif stlpecy == 13:
                y=np.append(y, np.array([e.snow]), axis=0)
            elif stlpecy == 14:
                y=np.append(y, np.array([e.thunderstorm]), axis=0)
            elif stlpecy == 15:
                y=np.append(y, np.array([e.ice_formation]), axis=0)
    
    if stlpecx == 1:
        xn="wind direction"
    elif stlpecx == 2:
        xn="average wind speed"
    elif stlpecx == 3:
        xn="temperature"
    elif stlpecx == 4:
        xn="duration of sunshine"
    elif stlpecx == 5:
        xn="global radiation"
    elif stlpecx == 6:
        xn="duration of precipitation"
    elif stlpecx == 7:
        xn="air pressure"
    elif stlpecx == 8:
        xn="horizontal view"
    elif stlpecx == 9:
        xn="overcast"
    elif stlpecx == 10:
        xn="hunidity"
    elif stlpecx == 11:
        xn="fog"
    elif stlpecx == 12:
        xn="rain"
    elif stlpecx == 13:
        xn="snow"
    elif stlpecx == 14:
        xn="thunderstorm"
    elif stlpecx == 15:
        xn="ice formation"
    
    if stlpecy == 1:
        yn="wind direction"
    elif stlpecy == 2:
        yn="average wind speed"
    elif stlpecy == 3:
        yn="temperature"
    elif stlpecy == 4:
        yn="duration of sunshine"
    elif stlpecy == 5:
        yn="global radiation"
    elif stlpecy == 6:
        yn="duration of precipitation"
    elif stlpecy == 7:
        yn="air pressure"
    elif stlpecy == 8:
        yn="horizontal view"
    elif stlpecy == 9:
        yn="overcast"
    elif stlpecy == 10:
        yn="hunidity"
    elif stlpecy == 11:
        yn="fog"
    elif stlpecy == 12:
        yn="rain"
    elif stlpecy == 13:
        yn="snow"
    elif stlpecy == 14:
        yn="thunderstorm"
    elif stlpecy == 15:
        yn="ice formation"
    
    #from sklearn.preprocessing import PolynomialFeatures
    #poly=PolynomialFeatures(degree=stupen)
    #X_poly=poly.fit_transform(x)
    #poly.fit(X_poly,y)
    #lin2 = LinearRegression()
    #lin2.fit(X_poly, y)
    
    #plt.scatter(X, y, color = 'blue')
 
    #plt.plot(X, lin2.predict(poly.fit_transform(X)), color = 'red')

    #plt.show()
    koef=np.polyfit(x, y, stupen)
    mymodel= np.poly1d(np.polyfit(x, y, stupen))

    myline = np.linspace(np.amin(x), np.amax(x), 100)
    
    plt.scatter(x, y)
    plt.plot(myline, mymodel(myline),color='red')
    #plt.plot(mymodel)
    tita="y = "
    #titb=", "
    titb=""
    for i in range(stupen,-1,-1):
        tita+=" + b"+str(i)+"x^"+str(i)
        titb+="\n b"+str(i)+" = "+str(koef[i])
    #tit=tita+titb
    tit=tita
    plt.title(tit)
    #plt.text(8, 13,titb,ha='left',va='top')
    vyska=0.8-stupen*0.02
    plt.figtext(0.65, vyska, titb, fontsize=7)
    plt.xlabel(xn)
    plt.ylabel(yn)
    plt.show() 
    return HttpResponse("Vykreslené v novom okne")

import numpy as np
import pandas as pd
import glob
from pykrige.ok import OrdinaryKriging
from pykrige.kriging_tools import write_asc_grid
import pykrige.kriging_tools as kt
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
from matplotlib.colors import LinearSegmentedColormap
from matplotlib.patches import Path, PathPatch
def ordkrigmap(request):
    date = request.GET["date"]
    hour = request.GET["hour"]
    stlpec = int(request.GET["stlpec"])
    lons=np.empty((0,1), float)
    lats=np.empty((0,1), float)
    data=np.empty((0,1), int)
    for e in MeteoStations.objects.all():
        
        query=Measurement.objects.get(measurement_date=date,measurement_hour=hour,station_id=e.id)
        if stlpec == 1:
            c=query.wind_direction
        elif stlpec == 2:
            c=query.average_wind_speed
        elif stlpec == 3:
            c=query.temperature
        elif stlpec == 4:
            c=query.duration_of_sunshine
        elif stlpec == 5:
            c=query.global_radiation
        elif stlpec == 6:
            c=query.duration_of_precipitation
        elif stlpec == 7:
            c=query.air_pressure
        elif stlpec == 8:
           c=query.horizontal_view
        elif stlpec == 9:
            c=querye.overcast
        elif stlpec == 10:
            c=query.humidity
        elif stlpec == 11:
            c=query.fog
        elif stlpec == 12:
            c=query.rain
        elif stlpec == 13:
            c=query.snow
        elif stlpec == 14:
            c=query.thunderstorm
        elif stlpec == 15:
            c=query.ice_formation
        if c is not None:
            lons=np.append(lons, np.array([[e.longitude]]), axis=0)
            lats=np.append(lats, np.array([[e.latitude]]), axis=0)
            data=np.append(data, np.array([[c]]), axis=0)
    grid_space = 0.01
    grid_lon = np.arange(np.amin(lons), np.amax(lons), grid_space)
    grid_lat = np.arange(np.amin(lats), np.amax(lats), grid_space)
    OK = OrdinaryKriging(lons, lats, data, variogram_model='gaussian', verbose=True, enable_plotting=False,nlags=20)
    z1, ss1 = OK.execute('grid', grid_lon, grid_lat)
    xintrp, yintrp = np.meshgrid(grid_lon, grid_lat)
    fig, ax = plt.subplots(figsize=(10,10))
    m = Basemap(llcrnrlon=lons.min()-0.1,llcrnrlat=lats.min()-0.1,urcrnrlon=lons.max()+0.1,urcrnrlat=lats.max()+0.1, projection='merc', resolution='h',area_thresh=1000.,ax=ax)
    
    m.drawcoastlines() #draw coastlines on the map
    x,y=m(xintrp, yintrp) # convert the coordinates into the map scales
    ln,lt=m(lons,lats)
    cs=ax.contourf(x, y, z1, np.linspace(np.amin(data), np.amax(data)),extend='both',cmap='jet') #plot the data on the map.
    cbar=m.colorbar(cs,location='right',pad="7%") #plot the colorbar on the map
    # draw parallels.
    parallels = np.arange(21.5,26.0,0.5)
    m.drawparallels(parallels,labels=[1,0,0,0],fontsize=14, linewidth=0.0) #Draw the latitude labels on the map

    # draw meridians
    meridians = np.arange(119.5,122.5,0.5)
    m.drawmeridians(meridians,labels=[0,0,0,1],fontsize=14, linewidth=0.0)
    plt.show()
    return HttpResponse("Vykreslené v novom okne")
from pykrige.uk import UniversalKriging
def unikrigmap(request):
    date = request.GET["date"]
    hour = request.GET["hour"]
    stlpec = int(request.GET["stlpec"])
    lons=np.empty((0,1), float)
    lats=np.empty((0,1), float)
    data=np.empty((0,1), int)
    for e in MeteoStations.objects.all():
        query=Measurement.objects.get(measurement_date=date,measurement_hour=hour,station_id=e.id)
        if stlpec == 1:
            c=query.wind_direction
        elif stlpec == 2:
            c=query.average_wind_speed
        elif stlpec == 3:
            c=query.temperature
        elif stlpec == 4:
            c=query.duration_of_sunshine
        elif stlpec == 5:
            c=query.global_radiation
        elif stlpec == 6:
            c=query.duration_of_precipitation
        elif stlpec == 7:
            c=query.air_pressure
        elif stlpec == 8:
           c=query.horizontal_view
        elif stlpec == 9:
            c=querye.overcast
        elif stlpec == 10:
            c=query.humidity
        elif stlpec == 11:
            c=query.fog
        elif stlpec == 12:
            c=query.rain
        elif stlpec == 13:
            c=query.snow
        elif stlpec == 14:
            c=query.thunderstorm
        elif stlpec == 15:
            c=query.ice_formation
        if c is not None:
            lons=np.append(lons, np.array([[e.longitude]]), axis=0)
            lats=np.append(lats, np.array([[e.latitude]]), axis=0)
            data=np.append(data, np.array([[c]]), axis=0)
    grid_space = 0.01
    grid_lon = np.arange(np.amin(lons), np.amax(lons), grid_space)
    grid_lat = np.arange(np.amin(lats), np.amax(lats), grid_space)
    UK = UniversalKriging(lons, lats, data, variogram_model='gaussian', verbose=True, enable_plotting=False,nlags=20)
    z1, ss1 = UK.execute('grid', grid_lon, grid_lat)
    xintrp, yintrp = np.meshgrid(grid_lon, grid_lat)
    fig, ax = plt.subplots(figsize=(10,10))
    m = Basemap(llcrnrlon=lons.min()-0.1,llcrnrlat=lats.min()-0.1,urcrnrlon=lons.max()+0.1,urcrnrlat=lats.max()+0.1, projection='merc', resolution='h',area_thresh=1000.,ax=ax)
    
    m.drawcoastlines() #draw coastlines on the map
    x,y=m(xintrp, yintrp) # convert the coordinates into the map scales
    ln,lt=m(lons,lats)
    cs=ax.contourf(x, y, z1, np.linspace(np.amin(data), np.amax(data)),extend='both',cmap='jet') #plot the data on the map.
    cbar=m.colorbar(cs,location='right',pad="7%") #plot the colorbar on the map
    # draw parallels.
    parallels = np.arange(21.5,26.0,0.5)
    m.drawparallels(parallels,labels=[1,0,0,0],fontsize=14, linewidth=0.0) #Draw the latitude labels on the map

    # draw meridians
    meridians = np.arange(119.5,122.5,0.5)
    m.drawmeridians(meridians,labels=[0,0,0,1],fontsize=14, linewidth=0.0)
    plt.show()
    return HttpResponse("Vykreslené v novom okne")    


    
