from django.contrib.auth.models import User
from rest_framework import serializers
from quickstart.models import *


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = MeteoStations
        fields = ('id','name','latitude','longitude')
